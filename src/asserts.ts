export const assert = (b : boolean, msg : string) => {
    if (!b) throw new Error(msg);
}

export const assertThrows = (f : Function, ...args) => {
    let threw = false;
    try {
        f(...args);
    } catch (e) {
        threw = true;
    }

    if (!threw) {
        throw new Error("Function didn't throw");
    }
}
