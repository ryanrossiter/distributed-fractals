
// https://stackoverflow.com/questions/11089732/display-image-from-blob-using-javascript-and-websockets
// public method for encoding an Uint8Array to base64
const encode = (input) => {
    var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var output = "";
    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    var i = 0;

    while (i < input.length) {
        chr1 = input[i++];
        chr2 = i < input.length ? input[i++] : Number.NaN; // Not sure if the index 
        chr3 = i < input.length ? input[i++] : Number.NaN; // checks are needed here

        enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;

        if (isNaN(chr2)) {
            enc3 = enc4 = 64;
        } else if (isNaN(chr3)) {
            enc4 = 64;
        }
        output += keyStr.charAt(enc1) + keyStr.charAt(enc2) +
                  keyStr.charAt(enc3) + keyStr.charAt(enc4);
    }
    return output;
}

const buildWorkFn = async () => {
    const workBundle = await (await fetch('js/work.bundle.js')).text();
    return `(...args) => {
        ${workBundle}
        return self.workFn(...args);
    } `;
}

const deployJob = async (job) => {
    job.public.name = 'Distributed Fractals';
    job.requirements.environment.offscreenCanvas = true;
    job.on('accepted', () => console.log("Job accepted", job.id));
    job.on('status', console.log);
    job.work.on('console', console.log);
    job.work.on('uncaughtException', console.error);

    return await job.exec();
}

const main = async () => {
    // @ts-ignore
    const { compute } = dcp;

    const workFn = await buildWorkFn();

    const data = [1];
    console.log(`Deploying job with ${data.length} slices...`);
    const job = compute.for(data, workFn, []);
    Object.assign(window, { job });

    const results = await deployJob(job);

    console.log("Done!", Array.from(results));
    const img = document.createElement('img');
    img.src = 'data:image/png;base64,' + encode(new Uint8Array(results[0]));
    document.body.appendChild(img);
}

window.addEventListener('DOMContentLoaded', () => {
    document.getElementById('start-button').addEventListener('click', main);
});
