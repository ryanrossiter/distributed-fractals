/// <reference path="../node_modules/ts-shader-loader/lib/index.d.ts" />

import './types/dcp-client';
import vertexSource from '../res/fractalShader.vs';
import fragmentSource from '../res/fractalShader.fs';

const getRenderingContext = () => {
    const canvas = new OffscreenCanvas(800, 800);
    const ctx = canvas.getContext('webgl2');

    return ctx;
}

const buildShaderProgram = (gl : WebGL2RenderingContext, vertexSource : string, fragmentSource : string) => {
    const vertexShader = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShader, vertexSource);
    gl.compileShader(vertexShader);

    if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
        let err = gl.getShaderInfoLog(vertexShader);

        throw new Error(`Failed to create vertex shader: ${err}`);
    }

    const fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShader, fragmentSource);
    gl.compileShader(fragmentShader);

    if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
        let err = gl.getShaderInfoLog(fragmentShader);

        throw new Error(`Failed to create fragment shader: ${err}`);
    }

    const program = gl.createProgram();
    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);
    gl.linkProgram(program);
    gl.detachShader(program, vertexShader);
    gl.detachShader(program, fragmentShader);
    gl.deleteShader(vertexShader);
    gl.deleteShader(fragmentShader);

    if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
        let err = gl.getProgramInfoLog(program);

        throw new Error(`Failed to create program: ${err}`);
    }

    return program;
}

declare interface FractalShader {
    program : WebGLProgram,
    attribLocations: {
        vertexPosition : number,
        // vertexColor : number,
    },
    uniformLocations: {
        // mouseDelta : WebGLUniformLocation,
        // projectionMatrix : WebGLUniformLocation,
        // modelViewMatrix : WebGLUniformLocation,
    }
}

const buildFractalShader = (gl : WebGL2RenderingContext) : FractalShader => {
    const program = buildShaderProgram(gl, vertexSource, fragmentSource);

    return {
        program,
        attribLocations: {
            vertexPosition: gl.getAttribLocation(program, 'pos'),
            // vertexColor: gl.getAttribLocation(program, 'aVertexColor'),
        },
        uniformLocations: {
            // mouseDelta: gl.getUniformLocation(program, 'mouse_delta'),
            // projectionMatrix: gl.getUniformLocation(program, 'uProjectionMatrix'),
            // modelViewMatrix: gl.getUniformLocation(program, 'uModelViewMatrix'),
        }
    };
}

const initPositionBuffer = (gl : WebGL2RenderingContext) => {
    const positionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

    const positions = new Float32Array([
        -1.0,  1.0,
         1.0,  1.0,
        -1.0, -1.0,
         1.0, -1.0,
    ]);

    gl.bufferData(gl.ARRAY_BUFFER, positions, gl.STATIC_DRAW);

    return positionBuffer;
}

const initColorBuffer = (gl : WebGL2RenderingContext) => {
    const colorbuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, colorbuffer);

    const colors = new Float32Array([
        0.0,  0.0,  0.0,  1.0,    // white
        1.0,  0.0,  0.0,  1.0,    // red
        0.0,  1.0,  0.0,  1.0,    // green
        1.0,  1.0,  0.0,  1.0,    // blue
    ]);

    gl.bufferData(gl.ARRAY_BUFFER, colors, gl.STATIC_DRAW);

    return colorbuffer;
}

const setupWebGL = () => {
    const gl = getRenderingContext();
    const shaderProgram = buildFractalShader(gl);

    // gl.enableVertexAttribArray(0);
    const positionBuffer = initPositionBuffer(gl);
    const colorBuffer = initColorBuffer(gl);

    const numComponents = 2;  // pull out 2 values per iteration
    const type = gl.FLOAT;    // the data in the buffer is 32bit floats
    const normalize = false;  // don't normalize
    const stride = 0;         // how many bytes to get from one set of values to the next
                              // 0 = use type and numComponents above
    const offset = 0;         // how many bytes inside the buffer to start from
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    gl.vertexAttribPointer(shaderProgram.attribLocations.vertexPosition,
        numComponents,
        type,
        normalize,
        stride,
        offset);
    gl.enableVertexAttribArray(shaderProgram.attribLocations.vertexPosition);

    // gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
    // gl.vertexAttribPointer(shaderProgram.attribLocations.vertexColor,
    //     4,
    //     type,
    //     normalize,
    //     stride,
    //     offset);
    // gl.enableVertexAttribArray(shaderProgram.attribLocations.vertexColor);

    gl.useProgram(shaderProgram.program);

    return { gl, shaderProgram };
}

let mouseX = 0, mouseY = 0;
const render = (gl : WebGL2RenderingContext, shaderProgram : FractalShader) => {
    // Set the clear color to darkish green.
    // gl.clearColor(0.0, 0.5, 0.0, 1.0);
    // Clear the context with the newly set color. This is
    // the function call that actually does the drawing.
    // gl.clear(gl.COLOR_BUFFER_BIT);
    
    // gl.uniform2f(shaderProgram.uniformLocations.mouseDelta, mouseX, mouseY);
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
}

Object.assign(self, {
    async workFn() {
        progress();

        console.log("starting")
        let start = Date.now();
        const { gl, shaderProgram } = setupWebGL();
        console.log("done setup");
        render(gl, shaderProgram);

        console.log("did it! took", Date.now() - start, "ms");

        // @ts-ignore
        const blob = await gl.canvas.convertToBlob();
        return Array.from(new Uint8Array(await blob.arrayBuffer()));
    }
});
