const path = require('path');

module.exports = {
    mode: 'development',
    entry: './out/work-main.js',
    output: {
        path: path.resolve(__dirname, 'www', 'js'),
        filename: 'work.bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.(glsl|vs|fs)$/,
                use: 'ts-shader-loader'
            }
        ]
    }
};
