/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./out/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./out/main.js":
/*!*********************!*\
  !*** ./out/main.js ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {\r\n    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }\r\n    return new (P || (P = Promise))(function (resolve, reject) {\r\n        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }\r\n        function rejected(value) { try { step(generator[\"throw\"](value)); } catch (e) { reject(e); } }\r\n        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }\r\n        step((generator = generator.apply(thisArg, _arguments || [])).next());\r\n    });\r\n};\r\nvar __generator = (this && this.__generator) || function (thisArg, body) {\r\n    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;\r\n    return g = { next: verb(0), \"throw\": verb(1), \"return\": verb(2) }, typeof Symbol === \"function\" && (g[Symbol.iterator] = function() { return this; }), g;\r\n    function verb(n) { return function (v) { return step([n, v]); }; }\r\n    function step(op) {\r\n        if (f) throw new TypeError(\"Generator is already executing.\");\r\n        while (_) try {\r\n            if (f = 1, y && (t = op[0] & 2 ? y[\"return\"] : op[0] ? y[\"throw\"] || ((t = y[\"return\"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;\r\n            if (y = 0, t) op = [op[0] & 2, t.value];\r\n            switch (op[0]) {\r\n                case 0: case 1: t = op; break;\r\n                case 4: _.label++; return { value: op[1], done: false };\r\n                case 5: _.label++; y = op[1]; op = [0]; continue;\r\n                case 7: op = _.ops.pop(); _.trys.pop(); continue;\r\n                default:\r\n                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }\r\n                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }\r\n                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }\r\n                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }\r\n                    if (t[2]) _.ops.pop();\r\n                    _.trys.pop(); continue;\r\n            }\r\n            op = body.call(thisArg, _);\r\n        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }\r\n        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };\r\n    }\r\n};\r\nvar _this = this;\r\n// https://stackoverflow.com/questions/11089732/display-image-from-blob-using-javascript-and-websockets\r\n// public method for encoding an Uint8Array to base64\r\nvar encode = function (input) {\r\n    var keyStr = \"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=\";\r\n    var output = \"\";\r\n    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;\r\n    var i = 0;\r\n    while (i < input.length) {\r\n        chr1 = input[i++];\r\n        chr2 = i < input.length ? input[i++] : Number.NaN; // Not sure if the index \r\n        chr3 = i < input.length ? input[i++] : Number.NaN; // checks are needed here\r\n        enc1 = chr1 >> 2;\r\n        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);\r\n        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);\r\n        enc4 = chr3 & 63;\r\n        if (isNaN(chr2)) {\r\n            enc3 = enc4 = 64;\r\n        }\r\n        else if (isNaN(chr3)) {\r\n            enc4 = 64;\r\n        }\r\n        output += keyStr.charAt(enc1) + keyStr.charAt(enc2) +\r\n            keyStr.charAt(enc3) + keyStr.charAt(enc4);\r\n    }\r\n    return output;\r\n};\r\nvar buildWorkFn = function () { return __awaiter(_this, void 0, void 0, function () {\r\n    var workBundle;\r\n    return __generator(this, function (_a) {\r\n        switch (_a.label) {\r\n            case 0: return [4 /*yield*/, fetch('js/work.bundle.js')];\r\n            case 1: return [4 /*yield*/, (_a.sent()).text()];\r\n            case 2:\r\n                workBundle = _a.sent();\r\n                return [2 /*return*/, \"(...args) => {\\n        \" + workBundle + \"\\n        return self.workFn(...args);\\n    } \"];\r\n        }\r\n    });\r\n}); };\r\nvar deployJob = function (job) { return __awaiter(_this, void 0, void 0, function () {\r\n    return __generator(this, function (_a) {\r\n        switch (_a.label) {\r\n            case 0:\r\n                job.public.name = 'Distributed Fractals';\r\n                job.requirements.environment.offscreenCanvas = true;\r\n                job.on('accepted', function () { return console.log(\"Job accepted\", job.id); });\r\n                job.on('status', console.log);\r\n                job.work.on('console', console.log);\r\n                job.work.on('uncaughtException', console.error);\r\n                return [4 /*yield*/, job.exec()];\r\n            case 1: return [2 /*return*/, _a.sent()];\r\n        }\r\n    });\r\n}); };\r\nvar main = function () { return __awaiter(_this, void 0, void 0, function () {\r\n    var compute, workFn, data, job, results, img;\r\n    return __generator(this, function (_a) {\r\n        switch (_a.label) {\r\n            case 0:\r\n                compute = dcp.compute;\r\n                return [4 /*yield*/, buildWorkFn()];\r\n            case 1:\r\n                workFn = _a.sent();\r\n                data = [1];\r\n                console.log(\"Deploying job with \" + data.length + \" slices...\");\r\n                job = compute.for(data, workFn, []);\r\n                Object.assign(window, { job: job });\r\n                return [4 /*yield*/, deployJob(job)];\r\n            case 2:\r\n                results = _a.sent();\r\n                console.log(\"Done!\", Array.from(results));\r\n                img = document.createElement('img');\r\n                img.src = 'data:image/png;base64,' + encode(new Uint8Array(results[0]));\r\n                document.body.appendChild(img);\r\n                return [2 /*return*/];\r\n        }\r\n    });\r\n}); };\r\nwindow.addEventListener('DOMContentLoaded', function () {\r\n    document.getElementById('start-button').addEventListener('click', main);\r\n});\r\n//# sourceMappingURL=main.js.map\n\n//# sourceURL=webpack:///./out/main.js?");

/***/ })

/******/ });