/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./out/work-main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./out/types/dcp-client.js":
/*!*********************************!*\
  !*** ./out/types/dcp-client.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("//# sourceMappingURL=dcp-client.js.map\n\n//# sourceURL=webpack:///./out/types/dcp-client.js?");

/***/ }),

/***/ "./out/work-main.js":
/*!**************************!*\
  !*** ./out/work-main.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\r\n/// <reference path=\"../node_modules/ts-shader-loader/lib/index.d.ts\" />\r\nvar __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {\r\n    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }\r\n    return new (P || (P = Promise))(function (resolve, reject) {\r\n        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }\r\n        function rejected(value) { try { step(generator[\"throw\"](value)); } catch (e) { reject(e); } }\r\n        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }\r\n        step((generator = generator.apply(thisArg, _arguments || [])).next());\r\n    });\r\n};\r\nvar __generator = (this && this.__generator) || function (thisArg, body) {\r\n    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;\r\n    return g = { next: verb(0), \"throw\": verb(1), \"return\": verb(2) }, typeof Symbol === \"function\" && (g[Symbol.iterator] = function() { return this; }), g;\r\n    function verb(n) { return function (v) { return step([n, v]); }; }\r\n    function step(op) {\r\n        if (f) throw new TypeError(\"Generator is already executing.\");\r\n        while (_) try {\r\n            if (f = 1, y && (t = op[0] & 2 ? y[\"return\"] : op[0] ? y[\"throw\"] || ((t = y[\"return\"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;\r\n            if (y = 0, t) op = [op[0] & 2, t.value];\r\n            switch (op[0]) {\r\n                case 0: case 1: t = op; break;\r\n                case 4: _.label++; return { value: op[1], done: false };\r\n                case 5: _.label++; y = op[1]; op = [0]; continue;\r\n                case 7: op = _.ops.pop(); _.trys.pop(); continue;\r\n                default:\r\n                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }\r\n                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }\r\n                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }\r\n                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }\r\n                    if (t[2]) _.ops.pop();\r\n                    _.trys.pop(); continue;\r\n            }\r\n            op = body.call(thisArg, _);\r\n        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }\r\n        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };\r\n    }\r\n};\r\nObject.defineProperty(exports, \"__esModule\", { value: true });\r\n__webpack_require__(/*! ./types/dcp-client */ \"./out/types/dcp-client.js\");\r\nvar fractalShader_vs_1 = __webpack_require__(/*! ../res/fractalShader.vs */ \"./res/fractalShader.vs\");\r\nvar fractalShader_fs_1 = __webpack_require__(/*! ../res/fractalShader.fs */ \"./res/fractalShader.fs\");\r\nvar getRenderingContext = function () {\r\n    var canvas = new OffscreenCanvas(800, 800);\r\n    var ctx = canvas.getContext('webgl2');\r\n    return ctx;\r\n};\r\nvar buildShaderProgram = function (gl, vertexSource, fragmentSource) {\r\n    var vertexShader = gl.createShader(gl.VERTEX_SHADER);\r\n    gl.shaderSource(vertexShader, vertexSource);\r\n    gl.compileShader(vertexShader);\r\n    if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {\r\n        var err = gl.getShaderInfoLog(vertexShader);\r\n        throw new Error(\"Failed to create vertex shader: \" + err);\r\n    }\r\n    var fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);\r\n    gl.shaderSource(fragmentShader, fragmentSource);\r\n    gl.compileShader(fragmentShader);\r\n    if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {\r\n        var err = gl.getShaderInfoLog(fragmentShader);\r\n        throw new Error(\"Failed to create fragment shader: \" + err);\r\n    }\r\n    var program = gl.createProgram();\r\n    gl.attachShader(program, vertexShader);\r\n    gl.attachShader(program, fragmentShader);\r\n    gl.linkProgram(program);\r\n    gl.detachShader(program, vertexShader);\r\n    gl.detachShader(program, fragmentShader);\r\n    gl.deleteShader(vertexShader);\r\n    gl.deleteShader(fragmentShader);\r\n    if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {\r\n        var err = gl.getProgramInfoLog(program);\r\n        throw new Error(\"Failed to create program: \" + err);\r\n    }\r\n    return program;\r\n};\r\nvar buildFractalShader = function (gl) {\r\n    var program = buildShaderProgram(gl, fractalShader_vs_1.default, fractalShader_fs_1.default);\r\n    return {\r\n        program: program,\r\n        attribLocations: {\r\n            vertexPosition: gl.getAttribLocation(program, 'pos'),\r\n        },\r\n        uniformLocations: {\r\n        // mouseDelta: gl.getUniformLocation(program, 'mouse_delta'),\r\n        // projectionMatrix: gl.getUniformLocation(program, 'uProjectionMatrix'),\r\n        // modelViewMatrix: gl.getUniformLocation(program, 'uModelViewMatrix'),\r\n        }\r\n    };\r\n};\r\nvar initPositionBuffer = function (gl) {\r\n    var positionBuffer = gl.createBuffer();\r\n    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);\r\n    var positions = new Float32Array([\r\n        -1.0, 1.0,\r\n        1.0, 1.0,\r\n        -1.0, -1.0,\r\n        1.0, -1.0,\r\n    ]);\r\n    gl.bufferData(gl.ARRAY_BUFFER, positions, gl.STATIC_DRAW);\r\n    return positionBuffer;\r\n};\r\nvar initColorBuffer = function (gl) {\r\n    var colorbuffer = gl.createBuffer();\r\n    gl.bindBuffer(gl.ARRAY_BUFFER, colorbuffer);\r\n    var colors = new Float32Array([\r\n        0.0, 0.0, 0.0, 1.0,\r\n        1.0, 0.0, 0.0, 1.0,\r\n        0.0, 1.0, 0.0, 1.0,\r\n        1.0, 1.0, 0.0, 1.0,\r\n    ]);\r\n    gl.bufferData(gl.ARRAY_BUFFER, colors, gl.STATIC_DRAW);\r\n    return colorbuffer;\r\n};\r\nvar setupWebGL = function () {\r\n    var gl = getRenderingContext();\r\n    var shaderProgram = buildFractalShader(gl);\r\n    // gl.enableVertexAttribArray(0);\r\n    var positionBuffer = initPositionBuffer(gl);\r\n    var colorBuffer = initColorBuffer(gl);\r\n    var numComponents = 2; // pull out 2 values per iteration\r\n    var type = gl.FLOAT; // the data in the buffer is 32bit floats\r\n    var normalize = false; // don't normalize\r\n    var stride = 0; // how many bytes to get from one set of values to the next\r\n    // 0 = use type and numComponents above\r\n    var offset = 0; // how many bytes inside the buffer to start from\r\n    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);\r\n    gl.vertexAttribPointer(shaderProgram.attribLocations.vertexPosition, numComponents, type, normalize, stride, offset);\r\n    gl.enableVertexAttribArray(shaderProgram.attribLocations.vertexPosition);\r\n    // gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);\r\n    // gl.vertexAttribPointer(shaderProgram.attribLocations.vertexColor,\r\n    //     4,\r\n    //     type,\r\n    //     normalize,\r\n    //     stride,\r\n    //     offset);\r\n    // gl.enableVertexAttribArray(shaderProgram.attribLocations.vertexColor);\r\n    gl.useProgram(shaderProgram.program);\r\n    return { gl: gl, shaderProgram: shaderProgram };\r\n};\r\nvar mouseX = 0, mouseY = 0;\r\nvar render = function (gl, shaderProgram) {\r\n    // Set the clear color to darkish green.\r\n    // gl.clearColor(0.0, 0.5, 0.0, 1.0);\r\n    // Clear the context with the newly set color. This is\r\n    // the function call that actually does the drawing.\r\n    // gl.clear(gl.COLOR_BUFFER_BIT);\r\n    // gl.uniform2f(shaderProgram.uniformLocations.mouseDelta, mouseX, mouseY);\r\n    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);\r\n};\r\nObject.assign(self, {\r\n    workFn: function () {\r\n        return __awaiter(this, void 0, void 0, function () {\r\n            var start, _a, gl, shaderProgram, blob, _b, _c, _d;\r\n            return __generator(this, function (_e) {\r\n                switch (_e.label) {\r\n                    case 0:\r\n                        progress();\r\n                        console.log(\"starting\");\r\n                        start = Date.now();\r\n                        _a = setupWebGL(), gl = _a.gl, shaderProgram = _a.shaderProgram;\r\n                        console.log(\"done setup\");\r\n                        render(gl, shaderProgram);\r\n                        console.log(\"did it! took\", Date.now() - start, \"ms\");\r\n                        return [4 /*yield*/, gl.canvas.convertToBlob()];\r\n                    case 1:\r\n                        blob = _e.sent();\r\n                        _c = (_b = Array).from;\r\n                        _d = Uint8Array.bind;\r\n                        return [4 /*yield*/, blob.arrayBuffer()];\r\n                    case 2: return [2 /*return*/, _c.apply(_b, [new (_d.apply(Uint8Array, [void 0, _e.sent()]))()])];\r\n                }\r\n            });\r\n        });\r\n    }\r\n});\r\n//# sourceMappingURL=work-main.js.map\n\n//# sourceURL=webpack:///./out/work-main.js?");

/***/ }),

/***/ "./res/fractalShader.fs":
/*!******************************!*\
  !*** ./res/fractalShader.fs ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (\"#version 100\\r\\n// https://github.com/Arukiap/Fractal-Xplorer/blob/rayMarching/shaders/basicShader.fs\\r\\n\\r\\n//Ray Marching constants\\r\\nconst int MAX_MARCHING_STEPS = 1024;\\r\\nconst lowp float MAX_DIST = 100.0;\\r\\nconst lowp float EPSILON = 0.0005;\\r\\nconst lowp float FOV = 150.0;\\r\\n\\r\\n//Fractal constants\\r\\nlowp float POWER = 11.0;\\r\\nconst lowp float BAILOUT = 50.0;\\r\\nconst int ITERATIONS = 10;\\r\\nconst int SIERPISNKI_ITERATIONS = 20;\\r\\nconst int COLORITERATIONS = 10;\\r\\n\\r\\n//Shader constants\\r\\nconst lowp float shadowIntensity = 1.0; // From 0.0 to 1.0 how strong you want the shadows to be\\r\\nconst lowp float shadowDiffuse = 1.0 - shadowIntensity;\\r\\nconst lowp float diffuseStrength = 1.5; // The higher the value the more bright the object gets if using normal lighting\\r\\nconst lowp float orbitStrength = 0.80; // The higher the value the more bright the object gets\\r\\nlowp vec4 orbitTrap = vec4(MAX_DIST); // Orbit trapping in order to shade or color fractals\\r\\nint currentSteps;\\r\\n\\r\\n\\r\\nlowp vec2 mouseDelta;\\r\\n\\r\\n// in vec4 gl_FragCoord;\\r\\n\\r\\nvarying lowp float vSelectedFractal;\\r\\nvarying lowp float vSystemTime;\\r\\nvarying lowp vec2 vSystemResolution;\\r\\nvarying lowp vec3 vCamera_pos;\\r\\nvarying lowp vec2 vMouse_delta;\\r\\n\\r\\n/*\\r\\n * Sphere distance estimator function where sphere.w represents the radius of the sphere\\r\\n * Used for debug and testing\\r\\n */\\r\\nlowp float sphereSDF(vec3 samplePoint) {\\r\\n    lowp vec4 sphere = vec4(0.0,1.0,5.0,1.0);\\r\\n    return length(samplePoint-sphere.xyz) - sphere.w;\\r\\n}\\r\\n\\r\\n/*\\r\\n * Simple xy plane distance estimator function\\r\\n * Used for debug and testing\\r\\n */\\r\\nlowp float planeSDF(vec3 samplePoint) {\\r\\n    return 0.2-samplePoint.z;\\r\\n}\\r\\n\\r\\n/*\\r\\n * Signed distance function for the estimation of the mandelbulb set\\r\\n */\\r\\nlowp float mandelbulbSDF(vec3 pos, bool isLight) {\\r\\n\\tif(!isLight) orbitTrap = vec4(MAX_DIST);\\r\\n\\tlowp vec3 z = pos;\\r\\n\\tlowp float dr = 1.0;\\r\\n\\tlowp float r = 0.0;\\r\\n\\tfor (int i = 0; i < ITERATIONS ; i++) {\\r\\n\\t\\tr = length(z);\\r\\n\\r\\n\\t\\tif (r>BAILOUT) break;\\r\\n\\r\\n\\t\\t// convert to polar coordinates\\r\\n\\t\\tlowp float theta = acos(z.z/r);\\r\\n\\t\\tlowp float phi = atan(z.y,z.x);\\r\\n\\t\\tdr =  pow( r, POWER-1.0 )*POWER*dr + 1.0;\\r\\n\\t\\t\\r\\n\\t\\t// scale and rotate the point\\r\\n\\t\\tlowp float zr = pow( r,POWER);\\r\\n\\t\\ttheta = theta*POWER;\\r\\n\\t\\tphi = phi*POWER;\\r\\n\\t\\t\\r\\n\\t\\t// convert back to cartesian coordinates\\r\\n\\t\\tz = zr*vec3(sin(theta)*cos(phi), sin(phi)*sin(theta), cos(theta));\\r\\n\\r\\n\\t\\tz+=pos;\\r\\n\\r\\n\\t\\tif (i<COLORITERATIONS && !isLight) orbitTrap = min(orbitTrap,abs(vec4(z.x,z.y,z.z,r*r)));\\r\\n\\t}\\r\\n\\treturn 0.5*log(r)*r/dr;\\r\\n}\\r\\n\\r\\n/*\\r\\n * Signed distance function for the estimation of the 3D Sierpinski Tetrahedron fractal\\r\\n */\\r\\nlowp float sierpinskiSDF(vec3 z, bool isLight)\\r\\n{\\r\\n\\tif(!isLight) orbitTrap = vec4(MAX_DIST);\\r\\n\\tlowp vec3 a1 = vec3(0.5,0.5,-0.5);\\r\\n\\tlowp vec3 a2 = vec3(-0.5,-0.5,-0.5);\\r\\n\\tlowp vec3 a3 = vec3(0.5,-0.5,0.5);\\r\\n\\tlowp vec3 a4 = vec3(-0.5,0.5,0.5);\\r\\n\\tlowp vec3 c;\\r\\n\\tlowp float dist, d;\\r\\n\\r\\n\\tfor (int n = 0; n < SIERPISNKI_ITERATIONS; n++) {\\r\\n\\t\\t c = a1; dist = length(z-a1);\\r\\n\\t     d = length(z-a2); if (d < dist) { c = a2; dist=d; }\\r\\n\\t\\t d = length(z-a3); if (d < dist) { c = a3; dist=d; }\\r\\n\\t\\t d = length(z-a4); if (d < dist) { c = a4; dist=d; }\\r\\n\\t\\tz = 2.0*z-c*(2.0-1.0);\\r\\n\\t\\tlowp float r = dot(z,z);\\r\\n\\t\\tif (n<COLORITERATIONS && !isLight) orbitTrap = min(orbitTrap,abs(vec4(z.x,z.y,z.z,r)));\\r\\n\\t}\\r\\n\\r\\n\\treturn length(z) * pow(2.0, float(-SIERPISNKI_ITERATIONS));\\r\\n}\\r\\n\\r\\n/*\\r\\n * Signed distance function for the estimation of Julia quaternion set\\r\\n */\\r\\nlowp float juliaSDF(vec3 pos, bool isLight) {\\r\\n\\tif(!isLight) orbitTrap = vec4(MAX_DIST);\\r\\n\\tlowp vec4 p = vec4(pos, 0.0);\\r\\n\\tlowp vec4 dp = vec4(1.0,0.0,0.0,0.0);\\r\\n\\tfor (int i = 0; i < ITERATIONS; i++) {\\r\\n\\t\\tdp = 2.0* vec4(p.x*dp.x-dot(p.yzw, dp.yzw), p.x*dp.yzw+dp.x*p.yzw+cross(p.yzw, dp.yzw));\\r\\n\\t\\tp = vec4(p.x*p.x-dot(p.yzw, p.yzw), vec3(2.0*p.x*p.yzw))+0.30;\\r\\n\\t\\tlowp float p2 = dot(p,p);\\r\\n\\t\\tif (i<COLORITERATIONS && !isLight) orbitTrap = min(orbitTrap, abs(vec4(p.xyz,p2)));\\r\\n\\t\\tif (p2 > BAILOUT) break;\\r\\n\\t}\\r\\n\\tlowp float r = length(p);\\r\\n\\treturn  0.5 * r * log(r) / length(dp);\\r\\n}\\r\\n\\r\\nlowp float mandelboxSDF(vec3 pos, bool isLight) {\\r\\n  if(!isLight) orbitTrap = vec4(MAX_DIST);\\r\\n  lowp float SCALE = 2.8;\\r\\n  lowp float MR2 = 0.2;\\r\\n\\r\\n  lowp vec4 scalevec = vec4(SCALE, SCALE, SCALE, abs(SCALE)) / MR2;\\r\\n  lowp float C1 = abs(SCALE-1.0), C2 = pow(abs(SCALE), float(1-ITERATIONS));\\r\\n\\r\\n  // distance estimate\\r\\n  lowp vec4 p = vec4(pos.xyz, 1.0), p0 = vec4(pos.xyz, 1.0);  // p.w is knighty's DEfactor\\r\\n  \\r\\n  for (int i=0; i<ITERATIONS; i++) {\\r\\n    p.xyz = clamp(p.xyz, -1.0, 1.0) * 2.0 - p.xyz;  // box fold: min3, max3, mad3\\r\\n    lowp float r2 = dot(p.xyz, p.xyz);  // dp3\\r\\n\\tif (i<COLORITERATIONS && !isLight) orbitTrap = min(orbitTrap, abs(vec4(p.xyz,r2)));\\r\\n    p.xyzw *= clamp(max(MR2/r2, MR2), 0.0, 1.0);  // sphere fold: div1, max1.sat, mul4\\r\\n    p.xyzw = p*scalevec + p0;  // mad4\\r\\n  }\\r\\n  return ((length(p.xyz) - C1) / p.w) - C2;\\r\\n}\\r\\n\\r\\n\\r\\n/*\\r\\n * Returns a rotation matrix for a rotation of theta degrees in the z axis.\\r\\n */\\r\\nlowp mat4 rotateZaxis(float theta) {\\r\\n    lowp float c = cos(theta);\\r\\n    lowp float s = sin(theta);\\r\\n\\r\\n    return mat4(\\r\\n        vec4(c, -s, 0, 0),\\r\\n        vec4(s, c, 0, 0),\\r\\n        vec4(0, 0, 1, 0),\\r\\n        vec4(0, 0, 0, 1)\\r\\n    );\\r\\n}\\r\\n\\r\\n/*\\r\\n * Returns a rotation matrix for a rotation of theta degrees in the y axis.\\r\\n */\\r\\nlowp mat4 rotateYaxis(float theta) {\\r\\n    lowp float c = cos(theta);\\r\\n    lowp float s = sin(theta);\\r\\n\\r\\n    return mat4(\\r\\n        vec4(c, 0, s, 0),\\r\\n        vec4(0, 1, 0, 0),\\r\\n        vec4(-s, 0, c, 0),\\r\\n        vec4(0, 0, 0, 1)\\r\\n    );\\r\\n}\\r\\n\\r\\n/*\\r\\n * Returns a rotation matrix for a rotation of theta degrees in the x axis.\\r\\n */\\r\\nlowp mat4 rotateXaxis(float theta) {\\r\\n    lowp float c = cos(theta);\\r\\n    lowp float s = sin(theta);\\r\\n\\r\\n    return mat4(\\r\\n        vec4(1, 0, 0, 0),\\r\\n        vec4(0, c, -s, 0),\\r\\n        vec4(0,s, c, 0),\\r\\n        vec4(0, 0, 0, 1)\\r\\n    );\\r\\n}\\r\\n\\r\\n/*\\r\\n * Represents the current scene as a conjunction of all SDFunctions we want to represent.\\r\\n */\\r\\nlowp float sceneSDF(vec3 samplePoint, bool isLight) {\\r\\n\\r\\n\\tlowp vec3 fractalPoint = ((rotateXaxis(-vMouse_delta.y*0.005)*\\r\\n\\t\\t\\t\\t\\t\\t\\trotateYaxis(-vMouse_delta.x*0.005)*\\r\\n\\t\\t\\t\\t\\t\\t\\trotateYaxis(0.0)*\\r\\n\\t\\t\\t\\t\\t\\t\\tvec4(samplePoint,1.0))).xyz;\\r\\n\\r\\n\\tif(abs(vSelectedFractal - 1.0) < 0.1){\\r\\n\\t\\treturn mandelbulbSDF(fractalPoint,isLight);\\r\\n\\t}\\r\\n\\tif(abs(vSelectedFractal - 2.0) < 0.1){\\r\\n\\t\\treturn sierpinskiSDF(fractalPoint,isLight);\\r\\n\\t}\\r\\n\\tif(abs(vSelectedFractal - 3.0) < 0.1){\\r\\n\\t\\treturn juliaSDF(fractalPoint,isLight);\\r\\n\\t}\\r\\n\\r\\n\\treturn mandelboxSDF(fractalPoint,isLight);\\r\\n}\\r\\n\\r\\n/*\\r\\n * Ray marching algorithm.\\r\\n * Returns aprox. distance to the scene from a certain point with a certain direction.\\r\\n */\\r\\nlowp float rayMarch(vec3 from, vec3 direction, bool isLight) {\\r\\n\\tlowp float totalDistance = 0.0;\\r\\n\\tfor (int steps=0; steps < MAX_MARCHING_STEPS; steps++) {\\r\\n\\t\\tlowp vec3 p = from + totalDistance * direction;\\r\\n\\t\\tlowp float distance = sceneSDF(p,isLight);\\r\\n\\t\\ttotalDistance += distance;\\r\\n\\t\\tif (distance > MAX_DIST || distance < EPSILON) break;\\r\\n\\t}\\r\\n\\tcurrentSteps = MAX_MARCHING_STEPS;\\r\\n\\treturn totalDistance;\\r\\n}\\r\\n\\r\\n/*\\r\\n * Helper function to find the direction of the ray to march to.\\r\\n */\\r\\nlowp vec3 rayDirection(float fov, vec2 size, vec2 fragCoord){\\r\\n    lowp vec2 xy = fragCoord - size / 2.0;\\r\\n    lowp float z = size.y / tan(radians(fov)/2.0);\\r\\n    return normalize(vec3(xy,z));\\r\\n}\\r\\n\\r\\n/*\\r\\n * Returns an aprox. normal vector to the given point in space.\\r\\n * Useful for lighting.\\r\\n */\\r\\nlowp vec3 getNormal(vec3 samplePoint, bool isLight){\\r\\n    lowp float distanceToPoint = sceneSDF(samplePoint,isLight);\\r\\n    lowp vec2 e = vec2(.01,0); //epsilon vector to facilitate calculating the normal\\r\\n\\r\\n    lowp vec3 n = distanceToPoint - vec3(\\r\\n        sceneSDF(samplePoint-e.xyy,isLight),\\r\\n        sceneSDF(samplePoint-e.yxy,isLight),\\r\\n        sceneSDF(samplePoint-e.yyx,isLight)\\r\\n    );\\r\\n\\r\\n    return normalize(n);\\r\\n}\\r\\n\\r\\n/*\\r\\n * Returns the amount of diffuse for a certain pixel.\\r\\n * Currently not being used to light fractals, instead we simply use orbital trap.\\r\\n */\\r\\nlowp float getLight(vec3 samplePoint){\\r\\n    lowp vec3 lightPosition = vec3(10.0,10.0,-10.0);\\r\\n    lowp vec3 light = normalize(lightPosition-samplePoint);\\r\\n    lowp vec3 normal = getNormal(samplePoint,true);\\r\\n\\r\\n    lowp float dif = clamp(dot(normal,light)*diffuseStrength,0.0,1.0);\\r\\n\\r\\n\\t// march a bit above the point else we get 0 distance from rayMarch\\r\\n    lowp float distanceToLightSource = rayMarch(samplePoint+normal*EPSILON*2.0,light,true); \\r\\n\\r\\n\\t// if distance to light source is less then the actual distance, this means we have an object in between and need to apply shadow on it\\r\\n    if(distanceToLightSource < length(lightPosition-samplePoint)) dif *= shadowDiffuse;\\r\\n\\r\\n    return dif;\\r\\n}\\r\\n\\r\\nvoid main(){\\t\\r\\n\\r\\n\\t// returns for each pixel the direction of the ray to march\\r\\n    lowp vec3 dir = rayDirection(FOV,vSystemResolution,gl_FragCoord.xy); \\r\\n\\r\\n\\t// defines where the camera/eye is in space\\r\\n\\tlowp vec3 eye = vCamera_pos;    \\r\\n\\t//vec3 eye = vec3(0.0,0.0,-4.0); \\r\\n    lowp float marchedDistance = rayMarch(eye,dir,false);\\r\\n\\r\\n\\tif(marchedDistance >= MAX_DIST){\\r\\n\\t\\tlowp float glow = float(currentSteps)/3.0;\\r\\n\\t\\tgl_FragColor = mix(vec4(0.0,0.0,0.0,0.0),vec4(1.0,1.0,1.0,1.0),glow*0.05);\\r\\n\\t\\t//gl_FragColor = vec4(0.612,0.816,1.0,0.0);\\r\\n\\t} else {\\r\\n\\t\\t\\r\\n\\t\\t// get intersection point in scene and retrieve the diffuse we need to apply\\r\\n\\t\\tlowp vec3 p = eye + dir * marchedDistance; \\r\\n\\t\\tlowp float diffuse = getLight(p);\\r\\n\\r\\n\\t\\tlowp vec4 baseColor = vec4(1.0,orbitTrap.z,orbitTrap.x,1.0)*orbitTrap.w*0.6+diffuse*0.6+0.2;\\r\\n        // lowp vec4 baseColor = vec4(orbitTrap.xzy,1.0)*orbitTrap.w+diffuse*0.5;\\r\\n\\t\\tgl_FragColor = mix(baseColor,vec4(0.0,0.0,0.0,0.0),clamp(marchedDistance*0.0 /* disable fog */, 0.0,1.0));\\r\\n\\r\\n\\t}\\r\\n}\");\n\n//# sourceURL=webpack:///./res/fractalShader.fs?");

/***/ }),

/***/ "./res/fractalShader.vs":
/*!******************************!*\
  !*** ./res/fractalShader.vs ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (\"#version 100\\r\\n// https://github.com/Arukiap/Fractal-Xplorer/blob/rayMarching/shaders/basicShader.vs\\r\\n\\r\\nattribute vec3 pos;\\r\\n\\r\\nvarying float vSystemTime;\\r\\nvarying float vSelectedFractal;\\r\\nvarying vec2 vSystemResolution;\\r\\nvarying vec3 vCamera_pos;\\r\\nvarying vec2 vMouse_delta;\\r\\n\\r\\n// uniform float systemTime;\\r\\n// uniform float selectedFractal;\\r\\n// uniform vec2 systemResolution;\\r\\n// uniform lowp vec3 camera_pos;\\r\\n// uniform lowp vec2 mouse_delta;\\r\\n\\r\\nvoid main(){\\r\\n    gl_Position = vec4(pos,1.0);\\r\\n    vSystemTime = 0.0;\\r\\n    vSystemResolution = vec2(800.0, 800.0);\\r\\n    vCamera_pos = vec3(0.0,0.0,-1.5);\\r\\n    vMouse_delta = vec2(0.0,0.0);\\r\\n    vSelectedFractal = 0.0;\\r\\n}\");\n\n//# sourceURL=webpack:///./res/fractalShader.vs?");

/***/ })

/******/ });