#version 100
attribute vec4 aVertexPosition;
attribute vec4 aVertexColor;

varying lowp vec4 vStartPos;

void main() {
  gl_Position = aVertexPosition;
  vStartPos = aVertexColor;
}