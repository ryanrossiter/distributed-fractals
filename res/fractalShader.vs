#version 100
// https://github.com/Arukiap/Fractal-Xplorer/blob/rayMarching/shaders/basicShader.vs

attribute vec3 pos;

varying float vSystemTime;
varying float vSelectedFractal;
varying vec2 vSystemResolution;
varying vec3 vCamera_pos;
varying vec2 vMouse_delta;

// uniform float systemTime;
// uniform float selectedFractal;
// uniform vec2 systemResolution;
// uniform lowp vec3 camera_pos;
// uniform lowp vec2 mouse_delta;

void main(){
    gl_Position = vec4(pos,1.0);
    vSystemTime = 0.0;
    vSystemResolution = vec2(800.0, 800.0);
    vCamera_pos = vec3(0.0,0.0,-1.5);
    vMouse_delta = vec2(0.0,0.0);
    vSelectedFractal = 0.0;
}