#version 100

varying lowp vec4 vStartPos;
void main() {
  vec2 startPos = vStartPos;
  gl_FragColor = vStartPos;
}